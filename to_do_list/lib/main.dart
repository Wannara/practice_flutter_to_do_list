import 'package:flutter/material.dart';
import 'package:to_do_list/screens/homepage.dart';
// import './model/todo_model.dart';
// import './model/db_model.dart';


void main() async{
  WidgetsFlutterBinding.ensureInitialized();
  // var db = DatabaseConnect();

  // await db.insertTodo(Todo(
  //   id: 1, 
  //   title: 'this is the simple todo', 
  //   creationDate: DateTime.now(), 
  //   isChecked: false));
  //   print(await db.getTodo());

  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Homepage(),
    );
  }
}

