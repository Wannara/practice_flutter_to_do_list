import 'package:flutter/material.dart';
import 'package:to_do_list/model/db_model.dart';
import 'package:to_do_list/model/todo_model.dart';
import 'package:to_do_list/widgets/todo_list.dart';
import 'package:to_do_list/widgets/user_input.dart';

class Homepage extends StatefulWidget {
  const Homepage({Key? key}) : super(key: key);

  @override
  State<Homepage> createState() => _HomepageState();
}

class _HomepageState extends State<Homepage> {
  var db = DatabaseConnect();
  void addItem(Todo todo) async{
    await db.insertTodo(todo);
    setState(() {});
  }
  void deleteItem(Todo todo)async{
    await db.daleteTodo(todo);
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color(0xFFF5EBFF),
      appBar: AppBar(
        title: const Text('The list I made today'),
      ),
      body: Column(
        children: [
          Todoloist(
            insertFunction: addItem,
            deleteFunction: deleteItem,
          ),
          UserInput(insertFunction: addItem,
          ),

        ],
      ),
    );
    
  }
}