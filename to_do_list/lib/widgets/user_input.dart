import 'package:flutter/material.dart';
import 'package:to_do_list/model/todo_model.dart';

class UserInput extends StatelessWidget {
  final TextController = TextEditingController();
  final Function insertFunction;
  UserInput({required this.insertFunction,Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 5,vertical: 8),
      // color: const Color(0xFFDAB5FF),
      child: Row(
        children: [
           Expanded(
            child: Container(
              padding: const EdgeInsets.symmetric(horizontal: 10),
              decoration: BoxDecoration(
                border: Border.all(color: Colors.black45),
                borderRadius: BorderRadius.circular(15)
              ),
              child: TextField(
                controller: TextController,
                decoration: const InputDecoration(
                  hintText: 'add new to do',
                  border: InputBorder.none,

                ),
              ),
              )
            ),
          const SizedBox(width: 10),
          GestureDetector(
            onTap: (){
             var myTodo = Todo(
                title: TextController.text,
                creationDate: DateTime.now(),
                isChecked: false);
               insertFunction(myTodo);
            },
            child: Container(
              decoration: BoxDecoration(
                 color: Theme.of(context).primaryColor,
                borderRadius: BorderRadius.circular(15)
              ),
              padding: const EdgeInsets.symmetric(horizontal: 25, vertical: 18),
              child: const Text(
                'Add', 
                style:  TextStyle(
                  color:Colors.white,
                  fontWeight: FontWeight.bold,
                  )
                )
              
          ))
        ],
      ),
    );
  }
}